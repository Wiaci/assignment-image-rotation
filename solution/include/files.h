#ifndef ASSIGNMENT_IMAGE_ROTATION_FILES_H
#define ASSIGNMENT_IMAGE_ROTATION_FILES_H

#include <stdbool.h>
#include <stdio.h>

bool close_file(FILE* f);
FILE* open_file_on_read(char* filename);
FILE* open_file_on_write(char* filename);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILES_H
