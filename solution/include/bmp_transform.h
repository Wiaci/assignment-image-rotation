#ifndef SRC_BMP_TRANSFORM_H
#define SRC_BMP_TRANSFORM_H

#include <stdbool.h>

bool rotate90counter_clock(char *src_filename, char *dest_filename);

#endif //SRC_BMP_TRANSFORM_H
