#include "../include/files.h"

FILE* open_file_on_read(char* filename) {
    return fopen(filename, "r");
}

FILE* open_file_on_write(char* filename) {
    return fopen(filename, "w");
}

bool close_file(FILE* f) {
    if (fclose(f) != 0) return false;
    return true;
}
