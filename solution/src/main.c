#include "../include/bmp_transform.h"

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // suppress 'unused parameters' warning
    rotate90counter_clock(argv[1], argv[2]);
    return 0;
}
