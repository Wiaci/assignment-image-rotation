#include "../include/rotate.h"
#include "../include/image.h"

struct image* rotate( struct image const source ) {
    struct image* rotated = image_create(source.height, source.width);
    for (int i = 0; i < source.width; i++) {
        for (int j = 0; j < source.height; j++) {
            uint64_t src_i = source.height - j - 1;
            uint64_t src_j = i;
            rotated->data[i * source.height + j] = source.data[src_i * source.width + src_j];
        }
    }
    return rotated;
}
