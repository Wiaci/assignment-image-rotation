#include <stdio.h>

#include "../include/bmp_transform.h"
#include "../include/bmp.h"
#include "../include/files.h"
#include "../include/rotate.h"

bool deserializing_errors_handler(enum read_status status) {
    if (status == READ_OK) return true;
    if (status == READ_INVALID_BITS) {
        printf("The only bit count supported is 24\n");
    } else if (status == READ_INVALID_HEADER) {
        printf("Header size is unsupportable!\n");
    } else if (status == READ_INVALID_SIGNATURE) {
        printf("Invalid file signature!\n");
    } else if (status == READ_TOO_SMALL_DATA_SIZE) {
        printf("Data section size is too small!\n");
    }
    return false;
}

bool serializing_errors_handler(enum write_status status) {
    if (status == WRITE_OK) return true;
    if (status == WRITE_ERROR) {
        printf("Serialization gone wrong...\n");
    }
    return false;
}

bool rotate90counter_clock(char *src_filename, char *dest_filename) {
    FILE* src_file = open_file_on_read(src_filename);
    if (!src_file) {
        printf("Source file opening error!\n");
        return false;
    }

    struct image* img;
    enum read_status r_status = from_bmp(src_file, &img);
    close_file(src_file);
    if (!deserializing_errors_handler(r_status)) {
        image_destroy(img);
        return false;
    }

    struct image* rotated = rotate(*img);
    image_destroy(img);
    FILE* dest_file = open_file_on_write(dest_filename);
    if (!dest_file) {
        printf("Destination file opening error!\n");
        return false;
    }

    enum write_status w_status = to_bmp(dest_file, rotated);
    close_file(dest_file);
    image_destroy(rotated);

    if (!serializing_errors_handler(w_status)) return false;



    return true;
}
