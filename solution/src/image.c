#include "../include/image.h"
#include "stdint.h"
#include <malloc.h>

struct image* image_create(uint64_t width, uint64_t height) {
    struct image *img = malloc(sizeof (struct image));
    img->width = width;
    img->height = height;
    img->data = malloc(sizeof(struct pixel) * height * width);
    return img;
}

void image_destroy(struct image *img) {
    if (!img) return;
    if (img->data) {
        free(img->data);
    }
    free(img);
}
